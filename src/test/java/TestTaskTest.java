import org.junit.jupiter.api.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TestTaskTest {

    private final TestTask test = new TestTask();
    private List<String> strings = Arrays.asList("Bob", "Alice", "Joe", "bob", "alice", "dEN");
    private List<String> someStringsIsNull = Arrays.asList("Bob", null, "Joe", "bob", null, "dEN");
    private List<String> listIsNull = null;
    ByteArrayOutputStream output = new ByteArrayOutputStream();
    PrintStream printStream = new PrintStream(output);

    @BeforeEach
    public void setUp() {
        System.setOut(printStream);
    }

    @Test
    public void testFirst(){
        test.printWords(strings);
        String actualOut = output.toString();
        String expectedOut = "Alice : 2\r\nBob : 2\r\nDen : 1\r\nJoe : 1\r\n";
        assertEquals(expectedOut, actualOut);
    }

    @Test
    public void testListIsNull() {
        test.printWords(listIsNull);
        String actualOut = output.toString();
        String expectedOut = "Пустой список\r\n";
        assertEquals(expectedOut, actualOut);
    }

    @Test
    public void nullValuesShouldBeOmitted() {
        test.printWords(someStringsIsNull);
        String actualOut = output.toString();
        String expectedOut = "Один или несколько элементов списка равны null или пустой строке\r\n";
        assertEquals(expectedOut, actualOut);
    }
    @AfterEach
    public void cleanUpStreams() {
        System.setOut(null);
    }
}