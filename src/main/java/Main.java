import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        TestTask test = new TestTask();
        List<String> strings = Arrays.asList("Bob", "Alice", "Joe", "bob", "alice", "dEN");
        List<String> stringsWithNull = Arrays.asList("Bob", null, "Joe", "bob", null, "dEN");
        List<String> arrayIsNull = null;
        test.printWords(strings);
        System.out.println("_______________");
        test.printWords(stringsWithNull);
        System.out.println("_______________");
        test.printWords(arrayIsNull);
    }
}
