import java.util.*;

public class TestTask {

    public void printWords (List<String> str) {
        List<String> strTmp = new ArrayList<>();
        HashMap <String, Integer> countWords = new HashMap<>();
        try {
            if (str == null) {
                throw new IllegalArgumentException ("Пустой список");
            }
            for (String word : str) {
                if (word == null || word == "" || word == " ") {
                    throw new IllegalArgumentException ("Один или несколько элементов списка равны null или пустой строке");
                }
                if ( countWords.containsKey(word.toLowerCase())) {
                    countWords.put(word.toLowerCase(), countWords.get(word.toLowerCase()) + 1);
                } else {
                    countWords.put(word.toLowerCase(), 1);
                }
            }
            for (String word : countWords.keySet()) {
                String s = Character.toUpperCase(word.charAt(0)) + word.toLowerCase().substring(1);
                strTmp.add(s);
            }
            Collections.sort(strTmp);
            for (String word : strTmp){
                Integer value = countWords.get(word.toLowerCase());
                System.out.println(word + " " + ":" + " " + value);
            }
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }
}
